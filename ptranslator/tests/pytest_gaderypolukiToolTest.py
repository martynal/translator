import pytest

from gaderypoluki.gaderypoluki import GaDeRyPoLuKi


@pytest.fixture
def translator():
    translator = GaDeRyPoLuKi()
    return translator


# iteracja i kolejne wywołania testów
@pytest.mark.parametrize("test_input, expected", [
    ("Ala", "Gug"),
    ("Gal", "Agu"),
    ("kot", "ipt"),
    ("wonsz", "wpnsz"),
    ("DOM","EPM"),
    ("WaKaCjE", "WgIgCjD")
])
def test_should_translate(translator, test_input, expected):
    assert translator.translate(test_input) == expected
